use std::iter;
use std::vec;

pub enum OneOrMany<T> {
    One(T),
    Many(Vec<T>),
}

pub enum OneOrManyIter<T> {
    One(iter::Once<T>),
    Many(vec::IntoIter<T>),
}

impl<T> Iterator for OneOrManyIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Self::One(once)  => once.next(),
            Self::Many(iter) => iter.next(),
        }
    }
}

impl<T> IntoIterator for OneOrMany<T> {
    type Item = T;
    type IntoIter = OneOrManyIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            Self::One(e)  => OneOrManyIter::One(iter::once(e)),
            Self::Many(v) => OneOrManyIter::Many(v.into_iter()),
        }
    }
}
