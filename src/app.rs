use std::thread;
use std::sync::Mutex;
use std::sync::mpsc::{channel, Sender};

use crate::session::{SessionManager, SessionManagerMessage};

/// The shared app state.
pub struct App {
    /// Allows to transmit messages to the session manager (main communication channel)
    session_manager_tx: Mutex<Sender<SessionManagerMessage>>,
}

impl App {
    pub fn new() -> Self {
        let session_manager_tx = Self::spawn_session_manager();

        Self { session_manager_tx: Mutex::new(session_manager_tx) }
    }

    /// Sends a message to the session manager.
    pub fn send_message(&mut self, message: SessionManagerMessage) {
        lock!(self.session_manager_tx, session_manager_tx, {
            session_manager_tx.send(message).unwrap();
        });
    }

    /// This function is the heart of everything: it spawns a thread which owns the session
    /// manager. Once the session manager is created, it listens for incoming messages forever.
    fn spawn_session_manager() -> Sender<SessionManagerMessage> {
        let (session_manager_tx, session_manager_rx) = channel();
        let thread_session_manager_tx = session_manager_tx.clone();

        thread::spawn(move || {
            let session_manager = SessionManager::new(
                thread_session_manager_tx,
                session_manager_rx
            );
            session_manager.handle_messages();
        });

        session_manager_tx
    }
}
