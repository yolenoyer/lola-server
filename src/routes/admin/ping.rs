use crate::api::ApiResponse;
use crate::security::AdminUser;

#[get("/admin/ping")]
pub fn route(_admin_user: AdminUser) -> ApiResponse {
    ApiResponse::success_no_payload()
}
