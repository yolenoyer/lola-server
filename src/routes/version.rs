use crate::api::ApiResponse;

#[get("/version")]
pub fn route() -> ApiResponse<&'static str> {
    ApiResponse::success(env!("CARGO_PKG_VERSION"))
}
