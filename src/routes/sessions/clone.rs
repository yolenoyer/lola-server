use std::sync::Mutex;

use rocket::serde::Serialize;
use rocket::State;

use crate::App;
use crate::api::ApiResponse;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;

#[derive(Serialize)]
pub struct Payload {
    session_id: SessionId,
}

#[post("/sessions/<session_id>/clone")]
pub fn route(session_id: SessionId, app: &State<Mutex<App>>) -> ApiResponse<Payload> {
    let result = send_message!(app, tx,
        SessionManagerMessage::CloneSession(session_id, tx)
    );

    match result {
        Ok(session_id) => ApiResponse::success(Payload { session_id }),
        Err(err) => err.into(),
    }
}
