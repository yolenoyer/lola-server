use std::sync::Mutex;

use rocket::State;

use crate::App;
use crate::api::ApiResponse;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;

#[get("/sessions/<session_id>/ping")]
pub fn route(
    session_id: SessionId,
    app: &State<Mutex<App>>
) -> ApiResponse {
    let result = send_message!(app, tx,
        SessionManagerMessage::PingSession(session_id, tx)
    );

    match result {
        Ok(()) => ApiResponse::success_no_payload(),
        Err(err) => err.into(),
    }
}
