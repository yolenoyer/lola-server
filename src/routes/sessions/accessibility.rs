use std::sync::Mutex;

use rocket::State;
use rocket::form::Form;

use crate::App;
use crate::api::ApiResponse;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;

#[derive(FromForm)]
pub struct Body {
    is_public: bool,
}

#[put("/sessions/<session_id>/accessibility", data="<body>")]
pub fn route(
    session_id: SessionId,
    body: Form<Body>,
    app: &State<Mutex<App>>
) -> ApiResponse {
    let result = send_message!(app, tx,
        SessionManagerMessage::SetSessionAccessibility(session_id, body.is_public, tx)
    );

    match result {
        Ok(_) => ApiResponse::success_no_payload(),
        Err(err) => err.into(),
    }
}
