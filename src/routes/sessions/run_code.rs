use std::sync::Mutex;

use rocket::State;
use rocket::serde::{Serialize, Deserialize, json::Json};

use crate::App;
use crate::api::ApiResponse;
use crate::session::SessionManagerMessage;
use crate::session::{SessionId, SessionCommand};

#[derive(Deserialize)]
pub struct Body {
    code: Vec<String>,
}

#[derive(Serialize)]
pub struct Payload {
    commands: Vec<SessionCommand>,
}

#[put("/sessions/<session_id>/run", format="json", data="<body>")]
pub fn route(
    session_id: SessionId,
    body: Json<Body>,
    app: &State<Mutex<App>>
) -> ApiResponse<Payload> {
    let result = send_message!(app, tx,
        SessionManagerMessage::RunCode(session_id, body.code.clone(), tx)
    );

    match result {
        Ok(commands) => ApiResponse::success(Payload { commands }),
        Err(err) => err.into(),
    }
}
