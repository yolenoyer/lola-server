use std::sync::Mutex;

use rocket::State;
use rocket::form::Form;
use rocket::serde::Serialize;

use crate::App;
use crate::api::ApiResponse;
use crate::serialize::SessionInformation;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;

#[derive(FromForm)]
pub struct Body {
    code: String,
}

#[derive(Serialize)]
pub struct Payload {
    session_information: SessionInformation,
}

#[put("/sessions/<session_id>/commands/<command_id>", data="<body>")]
pub fn route(
    session_id: SessionId,
    command_id: u64,
    body: Form<Body>,
    app: &State<Mutex<App>>
) -> ApiResponse<Payload> {
    let result = send_message!(app, tx,
        SessionManagerMessage::EditCommand(session_id, command_id, body.code.clone(), tx)
    );

    match result {
        Ok(session_information) => ApiResponse::success(Payload { session_information }),
        Err(err) => err.into(),
    }
}
