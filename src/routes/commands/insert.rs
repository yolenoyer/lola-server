use std::sync::Mutex;

use rocket::State;
use rocket::serde::{Serialize, Deserialize, json::Json};

use crate::App;
use crate::api::ApiResponse;
use crate::serialize::SessionInformation;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;

#[derive(Deserialize)]
pub struct Body {
    before_command_id: String,
    code: Vec<String>,
}

#[derive(Serialize)]
pub struct Payload {
    session_information: SessionInformation,
}

#[post("/sessions/<session_id>/commands", format="json", data="<body>")]
pub fn route(
    session_id: SessionId,
    body: Json<Body>,
    app: &State<Mutex<App>>
) -> ApiResponse<Payload> {
    let before_command_id: u64 = match body.before_command_id.parse() {
        Ok(id) => id,
        Err(_) => return ApiResponse::error("Invalid command id"),
    };

    let result = send_message!(app, tx,
        SessionManagerMessage::InsertCommands(
            session_id,
            before_command_id,
            body.code.clone(),
            tx
        )
    );

    match result {
        Ok(session_information) => ApiResponse::success(Payload { session_information }),
        Err(err) => err.into(),
    }
}
