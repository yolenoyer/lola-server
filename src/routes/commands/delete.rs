use std::sync::Mutex;

use rocket::State;
use rocket::serde::Serialize;

use crate::App;
use crate::api::ApiResponse;
use crate::serialize::SessionInformation;
use crate::session::SessionManagerMessage;
use crate::session::SessionId;

#[derive(Serialize)]
pub struct Payload {
    session_information: SessionInformation,
}

#[delete("/sessions/<session_id>/commands/<command_id>")]
pub fn route(session_id: SessionId, command_id: u64, app: &State<Mutex<App>>) -> ApiResponse<Payload> {
    let result = send_message!(app, tx,
        SessionManagerMessage::DeleteCommand(session_id, command_id, tx)
    );

    match result {
        Ok(session_information) => ApiResponse::success(Payload { session_information }),
        Err(err) => err.into(),
    }
}
