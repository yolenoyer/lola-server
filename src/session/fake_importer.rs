use std::fmt::Debug;
use std::path::PathBuf;

use lola::parse::ParsedInputSource;
use lola::context::Importer;

#[derive(Debug)]
pub struct FakeImporter {}

impl FakeImporter {
    pub fn new() -> Self {
        Self {}
    }
}

impl Importer for FakeImporter {
    fn get_code(&self, _path: &str, _caller_source: Option<ParsedInputSource>) -> Result<(String, Option<PathBuf>), String> {
        Err("Import is not available".to_string())
    }
}
