use crate::session::LolaSession;

/// The state of a session.
///
/// This enum is quite particular, because when the state is `Running`, then the ownership of the
/// the `LolaSession` is lost: the ownership is then given to a new thread which will run the given
/// lola code.
pub enum SessionState {
    Sleeping(LolaSession),
    Running,
}

impl SessionState {
    /// Consumes the object and returns the wrapped sleeping lola session.
    ///
    /// Panics if the session is not `SessionState::Sleeping(_)`.
    pub fn into_lola_session(self) -> LolaSession {
        match self {
            SessionState::Sleeping(lola_session) => lola_session,
            SessionState::Running => panic!("Session state is not sleeping."),
        }
    }
}
