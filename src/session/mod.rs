mod session_id;
mod session_state;
mod lola_session;
mod session_command;
mod fake_importer;
mod session_manager;

use chrono::NaiveDateTime;
pub use session_id::*;
pub use session_state::*;
pub use lola_session::*;
pub use session_command::*;
pub use session_manager::*;

use std::sync::mpsc::Sender;
use std::mem;
use std::thread;

use rand::{thread_rng, Rng};
use diesel::prelude::*;

use lola::context::ContextRef;

use crate::error::{ApiError, ApiResult};
use crate::serialize::{SessionInformation, SessionSummary};
use crate::misc::InsertAt;
use crate::misc::OneOrMany::{self, One, Many};
use crate::db::{
    lola_query,
    db_limits::DbLimits,
    models::{FetchSession, NewSession},
};
use crate::has_database;
use self::sendable_session::SendableSession;

/// Represents a public lola session; the id is the public id that can be referred in the web API
/// to refer to the session.
///
/// When the session is "sleeping", then the real lola session is owned. But when the session is
/// "running", then the ownership of the lola session is given to the runner thread, then given
/// back to the session thanks to the `SessionManagerMessage::FreeSession` message (watched in the
/// session manager thread).
pub struct Session {
    id: SessionId,
    state: SessionState,
    commands: Vec<SessionCommand>,
    is_public: bool,
    created_at: NaiveDateTime,
    updated_at: NaiveDateTime,
}

impl Session {
    /// Creates a new lola session, in a default sleeping state.
    pub fn new(id: SessionId, created_at: Option<NaiveDateTime>) -> Self {
        let now = chrono::Utc::now().naive_utc();
        let created_at = created_at.unwrap_or(now);

        Self {
            id,
            state: SessionState::Sleeping(LolaSession::new()),
            commands: vec![],
            is_public: false,
            created_at,
            updated_at: now,
        }
    }

    /// Retrieves a whole session from the database.
    fn from_database(session_id: &SessionId) -> ApiResult<Session> {
        let fetch_session;
        let fetch_commands;

        lock_db_connection!(connection, {
            fetch_session = lola_query::get_session(connection, session_id)?;
            fetch_commands = lola_query::get_session_commands(connection, session_id)?;
        });

        let mut session = Session::from(fetch_session);
        let mut lola_session = session.detach_lola_session().unwrap();
        let mut cxt = ContextRef::new(&mut lola_session.container, &mut lola_session.scope);

        let session_commands = fetch_commands
            .into_iter()
            .map(|fetch_command| {
                SessionCommand::from_eval(fetch_command.id, fetch_command.input, &mut cxt)
            })
        .collect::<Vec<_>>();

        session.attach_lola_session(lola_session).unwrap();
        session.push_command(Many(session_commands));

        Ok(session)
    }

    /// Tries to wake up the given session from the database, then runs a closure by giving to it a
    /// mutable reference to the session.
    fn wakeup_then_mut<S>(
        session_id: SessionId,
        tx: Sender<ApiResult<S>>,
        session_manager_tx: Sender<SessionManagerMessage>,
        f: impl FnOnce(&mut Session) + Send + 'static,
    )
    where
        S: Send + 'static,
    {
        thread::spawn(move || {
            send_err_to_tx!(tx, {
                let mut session = Session::from_database(&session_id)?;

                f(&mut session);

                // Send a message to the session manager in order to inform that the session is
                // available again:
                // SAFETY: the session is not shared with this thread
                let sendable_session = unsafe { session.into_sendable() };
                let message = SessionManagerMessage::ReplaceSession(sendable_session);
                session_manager_tx.send(message).unwrap();
            });
        });
    }

    /// Gets the session id.
    pub fn id(&self) -> &SessionId {
        &self.id
    }

    /// Sets the session id.
    pub fn set_id(&mut self, new_session_id: SessionId) {
        self.id = new_session_id;
    }

    /// Gets the accessibility of the session.
    pub fn is_public(&self) -> bool {
        self.is_public
    }

    /// Sets the accessibility of the session.
    pub fn set_public(&mut self, is_public: bool) {
        self.is_public = is_public;
    }

    /// Updates the `updated_at` timestamp.
    pub fn update_timestamp(&mut self) {
        self.updated_at = chrono::Utc::now().naive_utc();
    }

    /// Creates a structure containing public information about the session.
    pub fn get_information(&self) -> SessionInformation {
        SessionInformation {
            session_id: self.id.clone(),
            is_public: self.is_public,
            commands: self.commands.clone(),
            created_at: self.created_at,
            updated_at: self.updated_at,
        }
    }

    /// Creates a structure containing a public summary about the session.
    pub fn get_summary(&self) -> SessionSummary {
        SessionSummary {
            session_id: self.id.clone(),
        }
    }

    /// Gets the number of commands of the session.
    pub fn nb_commands(&self) -> usize {
        self.commands.len()
    }

    /// Resets the session.
    pub fn reset(&mut self) -> ApiResult {
        self.state = SessionState::Sleeping(LolaSession::new());
        self.commands = vec!();
        self.update_timestamp();
        Ok(())
    }

    /// Detaches the sleeping session, and sets the current state to `SessionState::Running`.
    pub fn detach_lola_session(&mut self) -> ApiResult<LolaSession> {
        match self.state {
            SessionState::Running => {
                Err(ApiError::custom(format!("Session {} is already running, aborting", self.id)))
            },
            SessionState::Sleeping(_) => {
                let mut new_state = SessionState::Running;
                mem::swap(&mut self.state, &mut new_state);
                Ok(new_state.into_lola_session())
            }
        }
    }

    /// Attaches the sleeping session, and sets the current state to `SessionState::Sleeping`.
    pub fn attach_lola_session(&mut self, lola_session: LolaSession) -> ApiResult {
        match self.state {
            SessionState::Sleeping(_) => {
                Err(ApiError::custom_internal(format!("Session {} is already attached, aborting", self.id)))
            },
            SessionState::Running => {
                self.state = SessionState::Sleeping(lola_session);
                Ok(())
            }
        }
    }

    /// Creates a new, available command id.
    ///
    /// NOTE: the availability of the id is actually not checked, since the probability of
    /// collision is incredibly weak.
    fn new_command_id(&self) -> u64 {
        // TODO: check collisions
        thread_rng().gen_range((u64::MAX >> 1) + 1 .. u64::MAX)
    }

    /// TODO: this check method implies an extra db connection lock
    fn check_can_add_commands(&self, code: &[impl AsRef<str>]) -> ApiResult {
        lock_db_connection!(connection, {
            connection.can_add_n_commands_in_session(&self.id, code.len())?;
        });
        for input in code {
            MysqlConnection::can_put_command(input.as_ref().len())?;
        }
        Ok(())
    }

    /// TODO: this check method implies an extra db connection lock
    fn check_can_edit_command(input: &str) -> ApiResult {
        MysqlConnection::can_put_command(input.len())
    }

    /// TODO: this check method implies an extra db connection lock
    fn check_can_clone_session(&self) -> ApiResult {
        lock_db_connection!(connection, {
            connection.can_clone_session(self.nb_commands())
        })
    }

    /// Fetches an entire session from the database and make it available.
    fn wakeup_session(
        session_id: SessionId,
        tx: Sender<ApiResult<SessionInformation>>,
        session_manager_tx: Sender<SessionManagerMessage>
    ) {
        thread::spawn(move || {
            send_err_to_tx!(tx, {
                let session = Session::from_database(&session_id)?;

                // Send the result to the transmitting channel:
                let result = Ok(session.get_information());
                tx.send(result).unwrap();

                // Send a message to the session manager in order to inform that the session is
                // available again (the session commands will be saved on message reception):
                // SAFETY: the session is not shared with this thread
                let sendable_session = unsafe { session.into_sendable() };
                let message = SessionManagerMessage::ReplaceSession(sendable_session);
                session_manager_tx.send(message).unwrap();
            });
        });
    }

    /// Runs the given lola code into the session.
    ///
    /// The resulting output (e.g: "7" for code="3+4") will be sent to `tx`.
    ///
    /// The `session_manager_tx` transmitter is needed to inform the session manager that the
    /// session is available again, once the code has been processed.
    fn run_code(
        &mut self,
        code: Vec<String>,
        tx: Sender<ApiResult<Vec<SessionCommand>>>,
        session_manager_tx: Sender<SessionManagerMessage>
    ) {
        send_err_to_tx!(tx, {
            self.check_can_add_commands(&code)?;

            let lola_session = self.detach_lola_session()?;
            // SAFETY: the lola session is not shared with this thread
            let sendable_lola_session = unsafe { lola_session.into_sendable() };

            let session_id = self.id.clone();
            let command_ids = (0..code.len()).map(|_| self.new_command_id()).collect::<Vec<_>>();

            thread::spawn(move || {
                let mut lola_session = sendable_lola_session.unpack();
                let mut cxt = ContextRef::new(&mut lola_session.container, &mut lola_session.scope);

                let session_commands = command_ids.into_iter().zip(code)
                    .map(|(command_id, code)| {
                        // Evaluate the code input and save the result into a session command:
                        SessionCommand::from_eval(command_id, code, &mut cxt)
                    })
                    .collect::<Vec<_>>();

                // Send the result to the transmitting channel:
                let result = Ok(session_commands.clone());
                tx.send(result).unwrap();

                // Send a message to the session manager in order to inform that the session is
                // available again (the session commands will be saved  on message reception):
                // SAFETY: the lola session is not shared with this thread
                let sendable_lola_session = unsafe { lola_session.into_sendable() };
                let message = SessionManagerMessage::FreeSession(
                    session_id,
                    sendable_lola_session,
                    session_commands
                );
                session_manager_tx.send(message).unwrap();
            });
        });
    }

    fn delete_command(
        &mut self,
        command_id_to_delete: u64,
        tx: Sender<ApiResult<SessionInformation>>,
        session_manager_tx: Sender<SessionManagerMessage>
    ) {
        send_err_to_tx!(tx, {
            // Destruct the old lola session:
            self.detach_lola_session()?;

            let session_id = self.id.clone();
            let is_public = self.is_public;
            let created_at = self.created_at;

            let mut commands = vec![];
            mem::swap(&mut self.commands, &mut commands);

            thread::spawn(move || {
                let mut new_session = Session::new(session_id, Some(created_at));
                new_session.set_public(is_public);
                let mut lola_session = new_session.detach_lola_session().unwrap();
                let mut cxt = ContextRef::new(&mut lola_session.container, &mut lola_session.scope);

                let new_inputs = commands.into_iter()
                    .filter(|command| command.command_id.0 != command_id_to_delete)
                    .map(|command| command.input);

                for input in new_inputs {
                    let command_id = new_session.new_command_id();
                    let session_command = SessionCommand::from_eval(command_id, input, &mut cxt);
                    new_session.push_command(One(session_command));
                }

                if has_database!() {
                    send_err_to_tx!(tx, {
                        new_session.update_db_commands()?;
                    });
                }

                new_session.attach_lola_session(lola_session).unwrap();

                // Send the result to the transmitting channel:
                let result = Ok(new_session.get_information());
                tx.send(result).unwrap();

                // Send the order to replace this old session (self) by the freshly created one, with
                // the wanted command deleted:
                // SAFETY: the session is not shared with this thread
                let sendable_session = unsafe { new_session.into_sendable() };
                let message = SessionManagerMessage::ReplaceSession(sendable_session);
                session_manager_tx.send(message).unwrap();
            });
        });
    }

    fn edit_command(
        &mut self,
        command_id_to_edit: u64,
        new_input: String,
        tx: Sender<ApiResult<SessionInformation>>,
        session_manager_tx: Sender<SessionManagerMessage>
    ) {
        send_err_to_tx!(tx, {
            Self::check_can_edit_command(&new_input)?;

            if !self.has_command(command_id_to_edit) {
                return Err(ApiError::custom(format!("Command {} not found", command_id_to_edit)));
            }

            // Destruct the old lola session:
            self.detach_lola_session()?;

            let session_id = self.id.clone();
            let is_public = self.is_public;
            let created_at = self.created_at;

            let mut commands = vec![];
            mem::swap(&mut self.commands, &mut commands);

            thread::spawn(move || {
                let mut new_session = Session::new(session_id, Some(created_at));
                new_session.set_public(is_public);
                let mut lola_session = new_session.detach_lola_session().unwrap();
                let mut cxt = ContextRef::new(&mut lola_session.container, &mut lola_session.scope);

                let new_inputs = commands.into_iter()
                    .map(move |command| {
                        if command.command_id.0 == command_id_to_edit {
                            new_input.clone()
                        } else {
                            command.input
                        }
                    });

                send_err_to_tx!(tx, {
                    new_session.add_inputs(new_inputs, &mut cxt)?;
                });

                new_session.attach_lola_session(lola_session).unwrap();

                // Send the result to the transmitting channel:
                let result = Ok(new_session.get_information());
                tx.send(result).unwrap();

                // Send the order to replace this old session (self) by the freshly created one, with
                // the wanted command deleted:
                // SAFETY: the session is not shared with this thread
                let sendable_session = unsafe { new_session.into_sendable() };
                let message = SessionManagerMessage::ReplaceSession(sendable_session);
                session_manager_tx.send(message).unwrap();
            });
        });
    }

    fn insert_commands(
        &mut self,
        before_command_id: u64,
        code: Vec<String>,
        tx: Sender<ApiResult<SessionInformation>>,
        session_manager_tx: Sender<SessionManagerMessage>
    ) {
        send_err_to_tx!(tx, {
            self.check_can_add_commands(&code)?;

            let insert_position = self.commands.iter()
                .position(|command| command.command_id.0 == before_command_id)
                .ok_or_else(|| {
                    ApiError::custom(format!("Command {} not found", before_command_id))
                })?;

            // Destruct the old lola session:
            self.detach_lola_session()?;

            let session_id = self.id.clone();
            let is_public = self.is_public;
            let created_at = self.created_at;

            let mut commands = vec![];
            mem::swap(&mut self.commands, &mut commands);

            thread::spawn(move || {
                let mut new_session = Session::new(session_id, Some(created_at));
                new_session.set_public(is_public);
                let mut lola_session = new_session.detach_lola_session().unwrap();
                let mut cxt = ContextRef::new(&mut lola_session.container, &mut lola_session.scope);

                let new_inputs = commands.into_iter()
                    .map(|command| command.input)
                    .insert_at(insert_position, Many(code));

                send_err_to_tx!(tx, {
                    new_session.add_inputs(new_inputs, &mut cxt)?;
                });

                new_session.attach_lola_session(lola_session).unwrap();

                // Send the result to the transmitting channel:
                let result = Ok(new_session.get_information());
                tx.send(result).unwrap();

                // Send the order to replace this old session (self) by the freshly created one, with
                // the wanted command deleted:
                // SAFETY: the session is not shared with this thread
                let sendable_session = unsafe { new_session.into_sendable() };
                let message = SessionManagerMessage::ReplaceSession(sendable_session);
                session_manager_tx.send(message).unwrap();
            });
        });
    }

    /// Clones the session, and sends the new session id.
    fn clone_session(
        &self,
        new_session_id: SessionId,
        tx: Sender<ApiResult<SessionId>>,
        session_manager_tx: Sender<SessionManagerMessage>
    ) {
        send_err_to_tx!(tx, {
            self.check_can_clone_session()?;

            let inputs = self.commands.iter()
                .map(|command| command.input.clone())
                .collect::<Vec<_>>();

            thread::spawn(move || {
                Self::clone_session_internal_(inputs, new_session_id, tx, session_manager_tx);
            });
        });
    }

    /// Retrieves a whole session from the database, makes it available, then clones it.
    fn wakeup_and_clone_session(
        session_id: SessionId,
        new_session_id: SessionId,
        tx: Sender<ApiResult<SessionId>>,
        session_manager_tx: Sender<SessionManagerMessage>,
    ) {
        thread::spawn(move || {
            send_err_to_tx!(tx, {
                let session = Session::from_database(&session_id)?;

                session.check_can_clone_session()?;

                let inputs = session.commands.iter()
                    .map(|command| command.input.clone())
                    .collect::<Vec<_>>();

                Self::clone_session_internal_(inputs, new_session_id, tx, session_manager_tx.clone());

                // Send a message to the session manager in order to inform that the session is
                // available again (the session commands will be saved on message reception):
                // SAFETY: the session is not shared with this thread
                let sendable_session = unsafe { session.into_sendable() };
                let message = SessionManagerMessage::ReplaceSession(sendable_session);
                session_manager_tx.send(message).unwrap();
            });
        });
    }

    /// Common code between `wakeup_and_clone_session()` and `clone_session()`.
    #[doc(hidden)]
    fn clone_session_internal_(
        inputs: Vec<String>,
        new_session_id: SessionId,
        tx: Sender<ApiResult<SessionId>>,
        session_manager_tx: Sender<SessionManagerMessage>
    ) {
        let mut new_session = Session::new(new_session_id, None);
        let mut lola_session = new_session.detach_lola_session().unwrap();
        let mut cxt = ContextRef::new(&mut lola_session.container, &mut lola_session.scope);

        for input in inputs {
            let command_id = new_session.new_command_id();
            let session_command = SessionCommand::from_eval(command_id, input, &mut cxt);
            new_session.push_command(One(session_command));
        }

        if has_database!() {
            send_err_to_tx!(tx, {
                use crate::db::schema::sessions::dsl::*;

                lock_db_connection!(connection, {
                    diesel::insert_into(sessions)
                        .values(&NewSession::from(&new_session))
                        .execute(connection)
                        .map_err(ApiError::from)?;
                });
                new_session.update_db_commands()?;
            })
        }

        new_session.attach_lola_session(lola_session).unwrap();

        // Send the result to the transmitting channel:
        let result = Ok(new_session.id.clone());
        tx.send(result).unwrap();


        // Send the order to replace this old session (self) by the freshly created one, with
        // the wanted command deleted:
        // SAFETY: the session is not shared with this thread
        let sendable_session = unsafe { new_session.into_sendable() };
        let message = SessionManagerMessage::ReplaceSession(sendable_session);
        session_manager_tx.send(message).unwrap();
    }

    #[cfg(feature = "database")]
    /// Rewrites entirely the commands of a session into the database.
    fn update_db_commands(&self) -> ApiResult {
        use crate::db::lola_query::SessionRef;

        lock_db_connection!(connection, {
            // Retrieve the session in order to get the database id:
            let fetch_session = lola_query::get_session(connection, &self.id)?;

            // Remove old commands:
            lola_query::delete_all_session_commands(connection, fetch_session.db_id)?;

            // Create the new commands:
            for (pos, command) in self.commands.iter().enumerate() {
                lola_query::insert_command(connection, command, fetch_session.db_id, pos as i32)?;
            }

            // Update the timestamp of the session:
            let session_ref = SessionRef::Id(&self.id);
            lola_query::update_session_timestamp(connection, session_ref, self.updated_at)?;
        });
        Ok(())
    }

    fn add_inputs(&mut self, inputs: impl Iterator<Item = String>, cxt: &mut ContextRef) -> ApiResult {
        for input in inputs {
            let command_id = self.new_command_id();
            let session_command = SessionCommand::from_eval(command_id, input, cxt);
            self.push_command(One(session_command));
        }
        self.update_timestamp();

        if has_database!() {
            self.update_db_commands()?;
        }

        Ok(())
    }

    /// Adds a command to the session.
    fn push_command(&mut self, commands: OneOrMany<SessionCommand>) {
        match commands {
            One(command) => self.commands.push(command),
            Many(mut commands) => self.commands.append(&mut commands),
        };
    }

    /// Returns true if the session has a command with the given id.
    fn has_command(&self, command_id: u64) -> bool {
        self.commands.iter().any(|command| command.command_id.0 == command_id)
    }

    /// Makes the session free again, by giving back the ownership of the lola session.
    fn free(&mut self, lola_session: LolaSession) {
        self.state = SessionState::Sleeping(lola_session);
    }

    unsafe fn into_sendable(self) -> SendableSession {
        SendableSession::new(self)
    }
}

impl From<FetchSession> for Session {
    fn from(fetch_session: FetchSession) -> Session {
        Self {
            id: SessionId::new_unchecked(fetch_session.id),
            state: SessionState::Sleeping(LolaSession::new()),
            commands: vec![],
            is_public: fetch_session.is_public,
            created_at: fetch_session.created_at,
            updated_at: fetch_session.updated_at,
        }
    }
}

pub mod sendable_session {
    use std::fmt;

    use super::Session;

    pub struct SendableSession(Session);

    // SAFETY: cannot be created without using the unsafe method new()
    unsafe impl Send for SendableSession {}

    impl SendableSession {
        pub unsafe fn new(session: Session) -> Self {
            Self(session)
        }

        pub fn unpack(self) -> Session {
            self.0
        }
    }

    impl fmt::Debug for SendableSession {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "(Sendable session)")
        }
    }
}
