use std::convert::TryFrom;
use std::fmt;
use std::ops::Deref;

use rocket::form::{self, ValueField};
use serde::ser::{Serialize, Serializer};
use rocket::{
    request::FromParam,
    form::FromFormField,
};


/// A session id.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct SessionId(String);

impl SessionId {
    /// Creates a new session id without validating it.
    pub fn new_unchecked(s: impl Into<String>) -> SessionId {
        SessionId(s.into())
    }

    /// Checks if the given string is a valid session id.
    pub fn is_valid_id(id: &str) -> bool {
        !id.is_empty() && id.chars().all(|c|
            ('a'..='z').contains(&c) ||
            ('A'..='Z').contains(&c) ||
            ('0'..='9').contains(&c) ||
            c == '-'
        )
    }

    /// Returns the inner str.
    pub fn inner_str(&self) -> &str {
        self.0.as_ref()
    }
}

impl Deref for SessionId {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Serialize for SessionId {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.0)
    }
}

impl<'r> FromParam<'r> for SessionId {
    type Error = SessionIdError;

    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        SessionId::try_from(param)
    }
}

impl<'v> FromFormField<'v> for SessionId {
    fn from_value(field: ValueField<'v>) -> form::Result<Self> {
        SessionId::try_from(field.value)
            .map_err(|e| form::Error::validation(e.to_string()).into())
    }
}

impl fmt::Display for SessionId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<u64> for SessionId {
    fn from(n: u64) -> SessionId {
        SessionId(n.to_string())
    }
}

impl TryFrom<&str> for SessionId {
    type Error = SessionIdError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let id = value.to_owned();

        if !Self::is_valid_id(&id) {
            return Err(SessionIdError::InvalidId(id));
        }

        Ok(SessionId(id))
    }
}

#[derive(Debug)]
pub enum SessionIdError {
    InvalidId(String),
}

impl std::error::Error for SessionIdError {}

impl fmt::Display for SessionIdError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::InvalidId(id) => write!(f, "Invalid session id: {}", id)
        }
    }
}
