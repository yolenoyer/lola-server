use rocket::{Request, http::Status, request::{FromRequest, Outcome}};
use rocket::outcome::IntoOutcome;

use crate::error::{AdminUserError, ApiError, ApiResult};

pub mod jwt;
pub mod hash;

pub struct AdminUser {
}

impl AdminUser {
    pub fn new() -> Self {
        Self {}
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AdminUser {
    type Error = ApiError;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        if let Err(err) = validate_token(req) {
            return Outcome::Failure((Status::Unauthorized, err));
        }

        Ok(AdminUser::new()).into_outcome(Status::Ok)
    }
}

pub fn validate_token(req: &Request<'_>) -> ApiResult {
    let jwt_string = match req.headers().get("Authorization").next() {
        Some(header) => parse_bearer(header)?,
        None => return Err(AdminUserError::NoAuthHeader.into()),
    };

    match jwt::validate(jwt_string) {
        Ok(_) => Ok(()),
        Err(err) => Err(AdminUserError::Jwt(err).into()),
    }
}

fn parse_bearer(input: &str) -> ApiResult<&str> {
    let mut words = input.trim().split(' ');
    if let Some(bearer) = words.next() {
        let bearer = bearer.to_ascii_lowercase();
        if bearer != "bearer" {
            return Err(AdminUserError::InvalidAuthHeader.into());
        }
    }
    words.last().ok_or_else(|| AdminUserError::InvalidAuthHeader.into())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_bearer() {
        macro_rules! assert_ok {
            ($input:expr, $expect:expr) => {{
                match parse_bearer($input) {
                    Ok(r) => assert_eq!(r, $expect),
                    e @ Err(_) => panic!("Ok({:?}) was expected, found {:?}", $expect, e),
                };
            }}
        }

        assert!(parse_bearer("").is_err());
        assert!(parse_bearer("bear").is_err());
        assert!(parse_bearer("bearer").is_err());
        assert!(parse_bearer("bearerabc").is_err());
        assert_ok!("bearer abc", "abc");
        assert_ok!("  bearer   abc", "abc");
        assert_ok!("  bEArer   abc  ", "abc");
    }
}
