use rand::{Rng, thread_rng};
use lazy_static::lazy_static;
use chrono::Local;
use serde::{Deserialize, Serialize};
use jsonwebtoken::{Algorithm, DecodingKey, EncodingKey, Header, TokenData, Validation, decode, encode};

const SECRET_KEY_SIZE: usize = 256;
const JWT_EXPIRATION_TIME: i64 = 86400; // In seconds

lazy_static! {
    pub static ref SECRET_KEY: [u8; SECRET_KEY_SIZE] = {
        let mut key = [0u8; SECRET_KEY_SIZE];
        for b in &mut key {
            *b = thread_rng().gen();
        }
        key
    };
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub name: String,
    pub exp: i64,
    pub iat: i64,
}

impl Claims {
    pub fn new(name: String) -> Self {
        let iat = Local::now().timestamp();
        Self {
            iat,
            exp: iat + JWT_EXPIRATION_TIME,
            name,
        }
    }
}

pub fn validate(token: &str) -> jsonwebtoken::errors::Result<TokenData<Claims>> {
    let secret_key = DecodingKey::from_secret(&*SECRET_KEY);
    let validation = Validation::new(Algorithm::HS256);
    decode::<Claims>(token, &secret_key, &validation)
}

pub fn create(name: impl Into<String>) -> String {
    let claims = Claims::new(name.into());
    let secret_key = EncodingKey::from_secret(&*SECRET_KEY);
    encode(&Header::default(), &claims, &secret_key).unwrap()
}
