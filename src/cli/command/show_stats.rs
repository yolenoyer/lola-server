use diesel::prelude::*;
use diesel::dsl::count_star;

use crate::error::ApiResult;

pub fn run() -> ApiResult {
    if has_database!() {
        println!("Database:");

        lock_db_connection!(connection, {
            use crate::db::schema::{sessions::dsl::*, commands::dsl::*};

            let nb_sessions: i64 = sessions
                .select(count_star())
                .first(connection)?;

            println!("  {} session(s)", nb_sessions);

            let nb_commands: i64 = commands
                .select(count_star())
                .first(connection)?;

            println!("  {} command(s)", nb_commands);
        });
    } else {
        eprintln!("Database feature is not available");
    };

    Ok(())
}
