use crate::security::hash::gen_salt;

pub fn run() {
    let salt = gen_salt();
    println!("{}", salt.as_str());
}
