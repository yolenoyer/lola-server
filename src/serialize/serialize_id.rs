use serde::ser::{Serialize, Serializer};

#[derive(Clone, Debug)]
pub struct SerializeId(pub u64);

impl From<u64> for SerializeId {
    fn from(id: u64) -> SerializeId {
        Self(id)
    }
}

impl Serialize for SerializeId {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = self.0.to_string();
        serializer.serialize_str(&s)
    }
}
