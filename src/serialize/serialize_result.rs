use serde::ser::{Serialize, Serializer, SerializeMap};

/// A wrapper for `Result` which customizes the serde serialization.
#[derive(Clone, Debug)]
pub struct SerializeResult<K, V> (pub Result<K, V>);

impl<K, V> Serialize for SerializeResult<K, V>
where
    K: Serialize,
    V: Serialize
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(1))?;
        match &self.0 {
            Ok(s) => map.serialize_entry("ok", s)?,
            Err(s)  => map.serialize_entry("error", s)?,
        }
        map.end()
    }
}

impl<K, V> From<Result<K, V>> for SerializeResult<K, V> {
    fn from(res: Result<K, V>) -> SerializeResult<K, V> {
        SerializeResult(res)
    }
}
