
use serde::Serialize;

use crate::session::SessionId;

/// Used to send public summary information about a session.
#[derive(Clone, Serialize)]
pub struct SessionSummary {
    pub session_id: SessionId,
}

