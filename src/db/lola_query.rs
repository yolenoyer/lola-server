use chrono::NaiveDateTime;
use diesel::MysqlConnection;
use diesel::{prelude::*, dsl::*};

use crate::db::models::{FetchSession, NewCommand};
use crate::db::schema::sessions;
use crate::db::schema::commands;
use crate::error::{ApiError, ApiResult, InternalError};
use crate::session::{SessionCommand, SessionId};

use super::models::FetchCommand;

pub fn ping(connection: &MysqlConnection) -> ApiResult {
    connection.execute("SELECT 1")
        .map(|_| ())
        .map_err(|e| e.into())
}

pub fn nb_sessions(connection: &MysqlConnection) -> ApiResult<i64> {
    use self::sessions::dsl::*;

    sessions.select(count_star())
        .first(connection)
        .map_err(|e| e.into())
}

pub fn nb_commands(connection: &MysqlConnection) -> ApiResult<i64> {
    use self::commands::dsl::*;

    commands.select(count_star())
        .first(connection)
        .map_err(|e| e.into())
}

pub fn nb_empty_sessions(connection: &MysqlConnection) -> ApiResult<i64> {
    use self::sessions::dsl::*;
    use self::commands::dsl as commands;

    sessions
        .select(count_star())
        .filter(
            not(db_id.eq_any(
                commands::commands
                .select(commands::session_db_id)
                .distinct()
            ))
        )
        .first(connection)
        .map_err(|e| e.into())
}

pub fn session_nb_commands(connection: &MysqlConnection, session_id: &SessionId) -> ApiResult<i64> {
    use self::commands::*;

    table
        .left_join(sessions::table.on(session_db_id.eq(sessions::db_id)))
        .filter(sessions::id.eq(&**session_id))
        .select(count_star())
        .first(connection)
        .map_err(|e| user_if_session_not_found(e, session_id))
}

pub fn get_session(connection: &MysqlConnection, session_id: &SessionId) -> ApiResult<FetchSession> {
    use self::sessions::dsl::*;

    sessions
        .filter(id.eq(session_id.inner_str()))
        .first::<FetchSession>(connection)
        .map_err(|e| user_if_session_not_found(e, session_id))
}

pub fn get_session_db_id(connection: &MysqlConnection, session_id: &SessionId) -> ApiResult<i32> {
    use self::sessions::dsl::*;

    sessions
        .select(db_id)
        .filter(id.eq(session_id.inner_str()))
        .first(connection)
        .map_err(|e| user_if_session_not_found(e, session_id))
}

pub fn has_session(connection: &MysqlConnection, session_id: &SessionId) -> ApiResult<bool> {
    use self::sessions::dsl::*;

    let count: i64 = sessions
        .select(count_star())
        .filter(id.eq(&**session_id))
        .first(connection)
        .map_err(|e| user_if_session_not_found(e, session_id))?;

    Ok(count >= 1)
}

pub fn delete_session(connection: &MysqlConnection, session_id: &SessionId) -> ApiResult {
    use self::sessions::dsl::*;

    delete(sessions.filter(id.eq(&**session_id)))
        .execute(connection)
        .map_err(|e| user_if_session_not_found(e, session_id))?;

    Ok(())
}

pub fn set_session_accessibility(
    connection: &MysqlConnection,
    session_id: &SessionId,
    timestamp: NaiveDateTime,
    value: bool,
) -> ApiResult {
    use self::sessions::dsl::*;

    update(sessions
        .filter(id.eq(session_id.inner_str())))
        .set((
            is_public.eq(value),
            updated_at.eq(timestamp),
        ))
        .execute(connection)
        .map_err(|e| user_if_session_not_found(e, session_id))?;

    Ok(())
}

pub fn update_session_timestamp(
    connection: &MysqlConnection,
    session_ref: SessionRef,
    timestamp: NaiveDateTime,
) -> ApiResult
{
    use self::sessions::dsl::*;

    match session_ref {
        SessionRef::Id(session_id) => {
            update(sessions.filter(id.eq(session_id.inner_str())))
                .set(updated_at.eq(timestamp))
                .execute(connection)
                .map_err(|e| user_if_session_not_found(e, session_id))?;
        }
        SessionRef::DbId(session_db_id) => {
            update(sessions.filter(db_id.eq(session_db_id)))
                .set(updated_at.eq(timestamp))
                .execute(connection)?;
        }
    }

    Ok(())
}

pub fn get_session_commands(
    connection: &MysqlConnection,
    session_id: &SessionId
) -> ApiResult<Vec<FetchCommand>>
{
    use self::commands::*;

    let fetch_commands = self::commands::dsl::commands
        .select((db_id, id, input, session_db_id, position))
        .left_join(sessions::table.on(session_db_id.eq(sessions::db_id)))
        .filter(sessions::id.eq(&**session_id))
        .load::<FetchCommand>(connection)
        .map_err(|e| user_if_session_not_found(e, session_id))?
        .into_iter()
        .collect::<Vec<_>>();

    Ok(fetch_commands)
}

pub fn insert_command(
    connection: &MysqlConnection,
    command: &SessionCommand,
    session_db_id: i32,
    pos: i32
) -> ApiResult
{
    let new_command = NewCommand::from_command(command, session_db_id, pos);
    insert_into(commands::table)
        .values(&new_command)
        .execute(connection)?;

    Ok(())
}

pub fn delete_all_session_commands(connection: &MysqlConnection, session_db_id: i32) -> ApiResult {
    delete(commands::table
        .filter(commands::session_db_id.eq(session_db_id)))
        .execute(connection)
        .map_err(ApiError::from)?;

    Ok(())
}

pub fn reset_session(
    connection: &MysqlConnection,
    session_id: &SessionId,
    updated_at: NaiveDateTime,
) -> ApiResult
{
    let session_db_id = get_session_db_id(connection, session_id)?;

    delete(commands::table
        .filter(commands::session_db_id.eq(session_db_id)))
        .execute(connection)
        .map_err(ApiError::from)?;

    let session_ref = SessionRef::DbId(session_db_id);
    update_session_timestamp(connection, session_ref, updated_at)?;

    Ok(())
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SessionRef<'a> {
    Id(&'a SessionId),
    DbId(i32),
}

// impl<'a> SessionRef<'a> {
//     pub fn get_id(&self, connection: &MysqlConnection) -> ApiResult<Cow<'a, SessionId>> {
//         use self::sessions::dsl::*;
//
//         match self {
//             Self::Id(v_id) => Ok(Cow::Borrowed(v_id)),
//             Self::DbId(v_db_id) => {
//                 let v_id: String = sessions
//                     .select(id)
//                     .filter(db_id.eq(v_db_id))
//                     .first(connection)?;
//                 let session_id = SessionId::new_unchecked(v_id);
//                 Ok(Cow::Owned(session_id))
//             }
//         }
//     }
//
//     pub fn get_db_id(&self, connection: &MysqlConnection) -> ApiResult<i32> {
//         use self::sessions::dsl::*;
//
//         match self {
//             Self::Id(v_id) => {
//                 let v_db_id: i32 = sessions
//                     .select(db_id)
//                     .filter(id.eq(v_id.inner_str()))
//                     .first(connection)?;
//                 Ok(v_db_id)
//             }
//             Self::DbId(v_db_id) => Ok(*v_db_id),
//         }
//     }
// }

/// If the error is `NotFound` then set the error as a user error. Otherwise, set as an internal
/// error.
fn user_if_session_not_found(err: diesel::result::Error, session_id: &SessionId) -> ApiError {
    match err {
        diesel::result::Error::NotFound => ApiError::SessionNotFound(session_id.clone()),
        _ => ApiError::Internal(InternalError::Query(err)),
    }
}
