use std::sync::Mutex;

use lazy_static::lazy_static;
use diesel::prelude::*;
use diesel::mysql::MysqlConnection;

use crate::error::{ApiError, ApiResult};

pub mod schema;
pub mod models;
pub mod lola_query;
pub mod db_limits;

lazy_static! {
    pub static ref DATABASE_URL: Mutex<Option<String>> = Mutex::new(None);
    pub static ref CONNECTION: Mutex<Option<MysqlConnection>> = Mutex::new(None);
}

#[macro_export]
macro_rules! has_database {
    () => { cfg!(feature = "database") };
}

#[macro_export]
macro_rules! lock_db_connection {
    ($conn:ident, $block:block) => {{
        let mut mutex_guard = (*$crate::db::CONNECTION).lock().unwrap();
        let conn = mutex_guard.as_ref().unwrap();
        if $crate::db::lola_query::ping(conn).is_err() {
            drop(mutex_guard);
            $crate::db::restart_connection()?;
            mutex_guard = (*$crate::db::CONNECTION).lock().unwrap();
        }
        let $conn = mutex_guard.as_ref().unwrap();
        $block
    }}
}

pub fn set_database_url(database_url: impl Into<String>) {
    let mut guard = (*DATABASE_URL).lock().unwrap();
    *guard = Some(database_url.into());
}

pub fn get_connection() -> ApiResult<MysqlConnection> {
    let database_url = (*DATABASE_URL).lock().unwrap();
    let database_url = (*database_url).as_ref().unwrap();
    MysqlConnection::establish(database_url)
        .map_err(|err| ApiError::custom_internal(format!("Error connecting to {}: {}", database_url, err)))
}

pub fn start_connection() -> ApiResult {
    let mut connection = (*CONNECTION).lock().unwrap();
    *connection = Some(get_connection()?);
    Ok(())
}

#[inline]
pub fn restart_connection() -> ApiResult {
    start_connection()
}
