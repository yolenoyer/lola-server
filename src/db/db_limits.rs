use diesel::MysqlConnection;

use crate::{error::{ApiError, ApiResult}, session::SessionId};
use super::lola_query;

pub const DB_LIMITS: DbLimitsValues = DbLimitsValues {
    nb_sessions: 2000,
    session_name_length: 256,
    nb_commands: 20_000,
    nb_commands_per_session: 100,
    command_length: 10_000,
};

pub struct DbLimitsValues {
    nb_sessions: i64,
    session_name_length: usize,
    nb_commands: i64,
    nb_commands_per_session: i64,
    command_length: usize,
}

pub trait DbLimits {
    fn can_add_session(&self, session_id: &SessionId) -> ApiResult;
    fn can_add_n_sessions(&self, nb_sessions_to_add: usize) -> ApiResult;
    fn can_set_session_name(&self, old_name: Option<&SessionId>, new_id: &SessionId) -> ApiResult;
    fn can_clone_session(&self, nb_commands: usize) -> ApiResult;
    fn can_add_n_commands(&self, nb_commands_to_add: usize) -> ApiResult;
    fn can_add_n_commands_in_session(&self, session_id: &SessionId, nb_commands: usize) -> ApiResult;
    fn can_add_command_in_session(&self, session_id: &SessionId, command_size: usize) -> ApiResult;
    fn can_put_command(command_length: usize) -> ApiResult;
}

impl DbLimits for MysqlConnection {
    fn can_add_session(&self, session_id: &SessionId) -> ApiResult {
        self.can_set_session_name(None, session_id)?;
        self.can_add_n_sessions(1)?;
        Ok(())
    }

    fn can_add_n_sessions(&self, nb_sessions_to_add: usize) -> ApiResult {
        let nb_sessions = lola_query::nb_sessions(self)?;
        let nb_sessions_to_add = nb_sessions_to_add as i64;

        if nb_sessions + nb_sessions_to_add > DB_LIMITS.nb_sessions {
            Err(ApiError::custom("Maximum number of sessions has been reached"))
        } else {
            Ok(())
        }
    }

    fn can_set_session_name(&self, _old_id: Option<&SessionId>, new_id: &SessionId) -> ApiResult {
        if lola_query::has_session(self, new_id)? {
            Err(ApiError::custom(format!("Session name '{}' already exists", new_id)))
        } else if new_id.len() > DB_LIMITS.session_name_length {
            Err(ApiError::custom("Session name is too long"))
        } else {
            Ok(())
        }
    }

    fn can_clone_session(&self, nb_commands_to_add: usize) -> ApiResult {
        self.can_add_n_sessions(1)?;
        self.can_add_n_commands(nb_commands_to_add)?;
        Ok(())
    }

    fn can_add_n_commands(&self, nb_commands_to_add: usize) -> ApiResult {
        let nb_commands = lola_query::nb_commands(self)?;
        let nb_commands_to_add = nb_commands_to_add as i64;

        if nb_commands + nb_commands_to_add > DB_LIMITS.nb_commands {
            Err(ApiError::custom("Maximum number of commands has been reached"))
        } else {
            Ok(())
        }
    }

    fn can_add_n_commands_in_session(&self, session_id: &SessionId, nb_commands_to_add: usize) -> ApiResult {
        self.can_add_n_commands(nb_commands_to_add)?;

        let session_nb_commands = lola_query::session_nb_commands(self, session_id)?;
        let nb_commands_to_add = nb_commands_to_add as i64;

        if session_nb_commands + nb_commands_to_add > DB_LIMITS.nb_commands_per_session {
            Err(ApiError::custom("Maximum number of commands has been reached for the session"))
        } else {
            Ok(())
        }
    }

    fn can_add_command_in_session(&self, session_id: &SessionId, command_length: usize) -> ApiResult {
        Self::can_put_command(command_length)?;
        self.can_add_n_commands_in_session(session_id, 1)?;
        Ok(())
    }

    fn can_put_command(command_length: usize) -> ApiResult {
        if command_length > DB_LIMITS.command_length {
            Err(ApiError::custom(format!("Command is too long, maximum {} bytes is allowed", DB_LIMITS.command_length)))
        } else {
            Ok(())
        }
    }
}
