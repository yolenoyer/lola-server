table! {
    commands (db_id) {
        db_id -> Integer,
        id -> Unsigned<Bigint>,
        input -> Mediumtext,
        session_db_id -> Integer,
        position -> Integer,
    }
}

table! {
    sessions (db_id) {
        db_id -> Integer,
        id -> Varchar,
        is_public -> Bool,
        created_at -> Datetime,
        updated_at -> Datetime,
    }
}

joinable!(commands -> sessions (session_db_id));

allow_tables_to_appear_in_same_query!(
    commands,
    sessions,
);
