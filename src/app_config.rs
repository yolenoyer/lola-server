use figment::{Figment, providers::{Env, Format, Toml}};
use serde::{Serialize, Deserialize};

pub const LOLA_SERVER_TOML_FILE: &str = "lola-server.toml";

pub fn lola_server_figment() -> Figment {
    Figment::from(rocket::Config::default())
        .merge(Toml::file(LOLA_SERVER_TOML_FILE))
        .merge(Env::prefixed("LOLA_SERVER_").global())
}

pub fn lola_server_config() -> Result<LolaServerConfig, figment::Error> {
    let figment = lola_server_figment();
    figment.extract()
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub struct LolaServerConfig {
    pub hash_salt: Option<String>,
    pub database_url: String,
    pub admin: Option<AdminConfig>,
}

#[derive(Debug, PartialEq, Clone, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct AdminConfig {
    pub username: String,
    pub password: String,
}
